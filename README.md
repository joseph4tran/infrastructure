# Description

This repository installs and configures the usegalaxy.fr instance of IFB

It will install:

- a nginx server on ubuntu with a configured proftpd service (using the geerlingguy.nginx and the galaxyproject.ansible-proftpd roles)
- a postgres server on centos (using the anxs.postgresql role)
- a galaxy server on centos (using the galaxyproject.galaxy and usegalaxy-eu.supervisor roles)

* Development instance: https://usegalaxy-dev.dev.ifb.local
* Preproduction instance: https://usegalaxy.ifb.local/
* Production instance: https://usegalaxy.fr


These playbooks assume the following steps have already been completed

* [x]  The ldap client, and ssh client were installed on the three servers
* [x]  An LDAP user "galaxy" exists
* [x]  The slurm client and moosefs client have been installed on the galaxy server
* [x]  DRMAA_LIBRARY_PATH var set to lib path to slurm-drmaa eg export DRMAA_LIBRARY_PATH=/shared/software/slurm-drmaa/1.0.8/libdrmaa.so.1.0.8
* [x]  The moose client was installed on the nginx server and galaxy directory was mounted

## Monitoring

[GTN - Monitoring](https://galaxyproject.github.io/training-material/topics/admin/tutorials/monitoring/tutorial.html)

To monitore Galaxy instances in real time (load, CPU, disk usage, running jobs ...).

### Telegraf

[Telegraf](https://github.com/influxdata/telegraf)

Telegraf get data from Galaxy (thanks to [statsd](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/statsd) plugin ) and store it in the InfluxDB database.
Telegraf is installed with the ansible role 'dj-wasabi.telegraf' in playbook_galaxy-extra.yml

#### Gxadmin

[GTN - gxadmin](https://galaxyproject.github.io/training-material/topics/admin/tutorials/gxadmin/tutorial.html)

To get statistics about other Galaxy-specific metrics such as the job queue status, we need to use gxadmin to query the Galaxy database and configure Telegraf to consume this data.
Gxadmin is installed with the ansible role 'usegalaxy-eu.gxadmin'

### InfluxDB

[InfluxDB](https://www.influxdata.com/) provides the data storage for monitoring. It is a TSDB, so it has been designed specifically for storing time-series data like monitoring and metrics. TSBDs commonly feature some form of automatic data expiration after a set period of time. In InfluxDB these are known as “retention policies”. Outside of this feature, it is a relatively normal database.
This database store data transfered from galaxy by Telegraf.
InfluxDB is installed with the ansible role 'usegalaxy_eu.influxdb'

### Grafana

[Grafana](https://www.influxdata.com/) get the Galaxy metrics from the Influx database to create his graphics. InfluxDB is updated every 10s thanks to Telegraf, allowing Grafana to render live graphics. Grafana dashboards are accessible at "https://{{ galaxy_instance }}/grafana/"
Grafana is installed with the ansible role 'cloudalchemy.grafana' in playbook_galaxy-monitoring.yml

## Gunicorn and Transparent restart

Starting with 22.05, we use Gunicorn instead of uWSGI. It means we can't use zergs anymore, and the transparent restart method (fifo stuff) has changed too.

As of 22.05, we are using the following stack:

- We have a `galaxy` systemd service
- The systemd service run Gravity commands (`galaxyctl`)
- Behind the scene, Gravity sets up its own supervisor config to run all the processes required by Galaxy
- One of the process is unicornherder, which is responsible for launching a Galaxy web process

Gunicorn can be launched (by unicornherder) in --preload mode or not. We don't run it in preload mode because it would mean that pid file would be created *after* Galaxy is up, and unicornherder doesn't like that (it thinks Gunicorn failed to launch and tries to rerun a new one, in a loop)

We use a fork of unicornherder, with specific timeouts to avoid errors while Gunicorn starts up (we consider it takes <180s to startup)

To restart Galaxy without downtime, tun this: `systemctl reload galaxy`. It will restart each process one by one, and finally unicornd will launch a new web process leaving the old running until the new one is up.

Galaxy reports is not working, because it requires gunicorn, but systemd services are not yet ready.

### After 22.05

Things that will need to be done in next versions:

- remove unicornherder, and adopt new stuff in gravity
- switch to preload mode probably
- review reports config with new gravity stuff
