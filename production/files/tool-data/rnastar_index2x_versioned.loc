#This is a sample file distributed with Galaxy that enables tools
#to use a directory of rna-star indexed sequences data files. You will
#need to create these data files and then create a rnastar_index2.loc
#file similar to this one (store it in this directory) that points to
#the directories in which those files are stored. The rnastar_index2.loc
#file has this format (longer white space characters are TAB characters):
#
#<unique_build_id>   <dbkey>   <display_name>   <file_base_path>        <with_gene_model>       <version>
#
#The <with_gene_model> column should be 1 or 0, indicating whether the index
#was built with annotations (i.e., --sjdbGTFfile and --sjdbOverhang were used)
#or not.
#
#The <version> column indicates the STAR version that introduced the format of
#the index, i.e., the oldest STAR version that could make use of the index.
#
#Note that STAR indices can become quite large. Consequently, it is only
#advisable to create indices with annotations if it's known ahead of time that
#(A) the annotations won't be frequently updated and (B) the read lengths used
#will also rarely vary. If either of these is not the case, it's advisable to
#create indices without annotations and then specify an annotation file and
#maximum read length (minus 1) when running STAR.
#
#hg19   hg19    hg19 full   /mnt/galaxyIndices/genomes/hg19/rnastar     0       2.7.1a
#hg19Ensembl   hg19Ensembl    hg19 full with Ensembl annotation   /mnt/galaxyIndices/genomes/hg19Ensembl/rnastar        1       2.7.1a

# commands
# find /shared/bank/ -type d -name "star*" > /tmp/star
# cat /tmp/star | sed -r "s|^(/shared/bank/)(.*)/(.*)/star-(.*)$|\3\t\3\t\2 (\3)\t\1\2/\3/star-\4\t0\t\4|"

GDDH13_v1.1	GDDH13_v1.1	malus_domestica (GDDH13_v1.1)	/shared/bank/malus_domestica/GDDH13_v1.1/star-2.7.5a	0	2.7.4a
12X	12X	vitis_vinifera (12X)	/shared/bank/vitis_vinifera/12X/star-2.7.5a	0	2.7.4a
Sscrofa11.1	Sscrofa11.1	sus_scrofa (Sscrofa11.1)	/shared/bank/sus_scrofa/Sscrofa11.1/star-2.7.5a	0	2.7.4a
rn6	rn6	Rat Rattus norvegicus (rn6)	/shared/bank/rattus_norvegicus/star-2.7.2b	0	2.7.2b
OBDH_1.0	OBDH_1.0	rosa_chinensis (OBDH_1.0)	/shared/bank/rosa_chinensis/OBDH_1.0/star-2.7.2b	0	2.7.2b
UCRNP2V3	UCRNP2V3	neofusicoccum_parvum (UCRNP2V3)	/shared/bank/neofusicoccum_parvum/UCRNP2V3/star-2.7.2b	0	2.7.2b
MG2	MG2	zymoseptoria_tritici (MG2)	/shared/bank/zymoseptoria_tritici/MG2/star-2.7.2b	0	2.7.2b
Astyanax_mexicanus-2.0	Astyanax_mexicanus-2.0	astyanax_mexicanus (Astyanax_mexicanus-2.0)	/shared/bank/astyanax_mexicanus/Astyanax_mexicanus-2.0/star-2.7.2b	0	2.7.2b
Astyanax_mexicanus-2.0	Astyanax_mexicanus-2.0	astyanax_mexicanus (Astyanax_mexicanus-2.0)	/shared/bank/astyanax_mexicanus/Astyanax_mexicanus-2.0/star-2.6	0	2.6
SL3.0	SL3.0	solanum_lycopersicum (SL3.0)	/shared/bank/solanum_lycopersicum/SL3.0/star-2.7.2b	0	2.7.2b
SL3.0	SL3.0	solanum_lycopersicum (SL3.0)	/shared/bank/solanum_lycopersicum/SL3.0/star-2.6	0	2.6
equcab3	equcab3	equus_caballus (equcab3)	/shared/bank/equus_caballus/equcab3/star-2.6.1d	0	2.6.1d
ASM294v2	ASM294v2	schizosaccharomyces_pombe (ASM294v2)	/shared/bank/schizosaccharomyces_pombe/ASM294v2/star-2.5.2b	0	2.5.2b
dm6	dm6	drosophila_melanogaster (dm6)	/shared/bank/drosophila_melanogaster/dm6/star-2.6.1a	0	2.6.1a
danRer10	danRer10	danio_rerio (danRer10)	/shared/bank/danio_rerio/danRer10/star-2.5.2b	0	2.5.2b
UMD3.1	UMD3.1	bos_taurus (UMD3.1)	/shared/bank/bos_taurus/UMD3.1/star-2.7.2b	0	2.7.2b
TAIR10.1	TAIR10.1	arabidopsis_thaliana (TAIR10.1)	/shared/bank/arabidopsis_thaliana/TAIR10.1/star-2.7.5a	0	2.7.4a
TAIR10.1	TAIR10.1	arabidopsis_thaliana (TAIR10.1)	/shared/bank/arabidopsis_thaliana/TAIR10.1/star-2.6.1a	0	2.6.1a
GRCh38	GRCh38	homo_sapiens (GRCh38)	/shared/bank/homo_sapiens/GRCh38/star-2.7.5a	0	2.7.4a
GRCh38	GRCh38	homo_sapiens (GRCh38)	/shared/bank/homo_sapiens/GRCh38/star-2.6.1a	0	2.6.1a
GRCh37	GRCh37	homo_sapiens (GRCh37)	/shared/bank/homo_sapiens/GRCh37/star-2.7.5a	0	2.7.4a
GRCh37	GRCh37	homo_sapiens (GRCh37)	/shared/bank/homo_sapiens/GRCh37/star-2.7.2b	0	2.7.2b
GRCh37	GRCh37	homo_sapiens (GRCh37)	/shared/bank/homo_sapiens/GRCh37/star-2.6	0	2.6
#hg38	hg38	homo_sapiens (hg38)	/shared/bank/homo_sapiens/hg38/star-2.7.5a	0	2.7.4a
hg38	hg38	homo_sapiens (hg38)	/shared/bank/homo_sapiens/hg38/star-2.7.2b	0	2.7.2b
hg38	hg38	homo_sapiens (hg38)	/shared/bank/homo_sapiens/hg38/star-2.6	0	2.6
#hg19	hg19	homo_sapiens (hg19)	/shared/bank/homo_sapiens/hg19/star-2.7.5a	0	2.7.4a
hg19	hg19	homo_sapiens (hg19)	/shared/bank/homo_sapiens/hg19/star-2.7.2b	0	2.7.2b
hg19	hg19	homo_sapiens (hg19)	/shared/bank/homo_sapiens/hg19/star-2.6	0	2.6
GRCm38	GRCm38	mus_musculus (GRCm38)	/shared/bank/mus_musculus/GRCm38/star-2.7.5a	0	2.7.4a
GRCm38	GRCm38	mus_musculus (GRCm38)	/shared/bank/mus_musculus/GRCm38/star-2.7.2b	0	2.7.2b
GRCm38	GRCm38	mus_musculus (GRCm38)	/shared/bank/mus_musculus/GRCm38/star-2.6	0	2.6
#mm9	mm9	mus_musculus (mm9)	/shared/bank/mus_musculus/mm9/star-2.7.5a	0	2.7.4a
mm9	mm9	mus_musculus (mm9)	/shared/bank/mus_musculus/mm9/star-2.7.2b	0	2.7.2b
mm9	mm9	mus_musculus (mm9)	/shared/bank/mus_musculus/mm9/star-2.4.0j	0	2.4.0j
#mm10	mm10	mus_musculus (mm10)	/shared/bank/mus_musculus/mm10/star-2.7.5a	0	2.7.4a
mm10	mm10	mus_musculus (mm10)	/shared/bank/mus_musculus/mm10/star-2.7.2b	0	2.7.2b
mm10	mm10	mus_musculus (mm10)	/shared/bank/mus_musculus/mm10/star-2.6	0	2.6

hg38	hg38	Human (Homo sapiens) (b38): hg38	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/hg38/hg38/dataset_fdfceb86-2b00-45f3-a3f4-63794ea8182d_files	0	2.7.4a
hg19	hg19	Human (Homo sapiens) (b37): hg19	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/hg19/hg19/dataset_d34482b9-5123-4ac6-b22d-89a2c5588299_files	0	2.7.4a
mm10	mm10	Mouse (Mus Musculus): mm10	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/mm10/mm10/dataset_5e31b655-e1b3-4c90-ae18-bbafb09a15fc_files	0	2.7.4a
rn6	rn6	Rat Jul. 2014 (RGSC 6.0/rn6) (rn6)	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/rn6/rn6/dataset_d0061d40-7013-4497-b3f7-7d57b2a1a38c_files	0	2.7.4a
dm6	dm6	D. melanogaster Aug. 2014 (BDGP Release 6 + ISO1 MT/dm6) (dm6)	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/dm6/dm6/dataset_d5ef3124-4a1d-46a0-886d-9e2e443c840f_files	0	2.7.4a
ce10	ce10	C. elegans (WS220): ce10	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/ce10/ce10/dataset_4acc8f48-c175-418c-9ce3-2f47447519f9_files	0	2.7.4a
dm3	dm3	Fruit Fly (Drosophila melanogaster): dm3	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/dm3/dm3/dataset_10797bbe-186d-4c6a-b139-e271fd641137_files	0	2.7.4a
mm9	mm9	Mouse (Mus musculus): mm9	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/mm9/mm9/dataset_13ca02f1-47ac-4793-969f-103e1ec04c24_files	0	2.7.4a
sacCer3	sacCer3	Yeast (Saccharomyces cerevisiae): sacCer3	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/sacCer3/sacCer3/dataset_ee6db620-7c48-4da0-8451-017d274c97ca_files	0	2.7.4a
sacCer2	sacCer2	Yeast (Saccharomyces cerevisiae): sacCer2	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/sacCer2/sacCer2/dataset_166fc7cd-9ff8-4acf-9614-a267eeaf9355_files	0	2.7.4a
ce9	ce9	C. elegans (WS210): ce9	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/ce9/ce9/dataset_d26ede5d-92f0-481e-af5a-df1b27e7a6fe_files	0	2.7.4a
rn5	rn5	Rat (Rattus norvegicus): rn5	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/rn5/rn5/dataset_ce95c5ad-dcdc-421a-af7f-f5680922d395_files	0	2.7.4a
apiMel4	apiMel4	A. mellifera 04 Nov 2010 (Amel_4.5/apiMel4) (apiMel4)	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/apiMel4/apiMel4/dataset_95aec843-8c04-4d1e-ab44-6888c1fdfefb_files	0	2.7.4a
Amel_4.5	Amel_4.5	A. mellifera Nov. 2010 (GCF_000002195.4/Amel_4.5) (Amel_4.5)	/cvmfs/data.galaxyproject.org/managed/rnastar/2.7.4a/Amel_4.5/Amel_4.5/dataset_c53a2b2d-8347-4188-82ad-aed87c416f38_files	0	2.7.4a
