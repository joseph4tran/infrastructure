---
slurm:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    # maximum resources that a single job can ask
    cores: 40
    mem: 1000
  env:
    GALAXY_MEMORY_MB: '{MEMORY_MB}'
    GALAXY_SLOTS: '{PARALLELISATION}'
  params:
    nativeSpecification: '-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    tmp_dir: 'True'

slurm_long_48:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    # maximum resources that a single job can ask
    cores: 40
    mem: 1000
  env:
    GALAXY_MEMORY_MB: '{MEMORY_MB}'
    GALAXY_SLOTS: '{PARALLELISATION}'
  params:
    nativeSpecification: '-p long -c {PARALLELISATION} --mem={MEMORY_MB} --time=48:00:00 {NATIVE_SPEC_EXTRA}'
    tmp_dir: 'True'

slurm_extra_long_168:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    # maximum resources that a single job can ask
    cores: 40
    mem: 1000
  env:
    GALAXY_MEMORY_MB: '{MEMORY_MB}'
    GALAXY_SLOTS: '{PARALLELISATION}'
  params:
    nativeSpecification: '-p long -c {PARALLELISATION} --mem={MEMORY_MB} --time=168:00:00 {NATIVE_SPEC_EXTRA}'
    tmp_dir: 'True'

slurm_singularity:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    cores: 40
    mem: 1000
  env: { }
  params:
    nativeSpecification: '-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    singularity_enabled: true
    require_container: true
    singularity_volumes: '$galaxy_root:ro,$tool_directory:ro,$job_directory:rw,$working_directory:rw,$default_file_path:rw'
    singularity_default_container_id: '/cvmfs/singularity.galaxyproject.org/all/ubuntu:20.04'
    tmp_dir: 'True'

slurm_singularity_gpu:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    cores: 40
    mem: 1000
  env: { }
  params:
    nativeSpecification: '-p gpu --gres=gpu:1g.5gb:1 -c {PARALLELISATION} --mem={MEMORY_MB} --time=48:00:00 {NATIVE_SPEC_EXTRA}'
    singularity_enabled: true
    require_container: true
    singularity_volumes: '$galaxy_root:ro,$tool_directory:ro,$job_directory:rw,$working_directory:rw,$default_file_path:rw'
    singularity_default_container_id: '/cvmfs/singularity.galaxyproject.org/all/ubuntu:20.04'
    tmp_dir: 'True'

pulsar_embedded_docker_gxit:
  info:
    remote: 'False'
    scheduler: 'pulsar_embedded'
  limits:
    cores: 10
    mem: 50
  env:
    # Need these for the gxit monitor script that calls the api from compute node
    SSL_CERT_FILE: '/etc/pki/tls/certs/ca-bundle.trust.crt'
    REQUESTS_CA_BUNDLE: '/etc/pki/tls/certs/ca-bundle.trust.crt'
  params:
    nativeSpecification: '-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    docker_enabled: true
    require_container: true
    docker_volumes: '$defaults'
    docker_net: 'bridge'
    docker_memory: '{MEMORY}'
    docker_auto_rm: true
    docker_default_container_id: busybox:ubuntu-14.04
    docker_sudo: false
    docker_set_user: ''
    container_monitor_result: 'callback'
    tmp_dir: 'True'

## For MPI mode
slurm_mpi:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    cores: 40
    mem: 1000
  env:
    GALAXY_MEMORY_MB: '{MEMORY_MB}'
    GALAXY_SLOTS: '{PARALLELISATION}'
  params:
    # MPI mode: run as many tasks as cpu asked
    nativeSpecification: '-p fast -N 1 -n {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    tmp_dir: 'True'

# --- Below: not used so far at usegalaxy.fr ---
## Install conda package within a singularity image
slurm_singularity_with_conda:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    cores: 40
    mem: 1000
  env:
    GALAXY_MEMORY_MB: '{MEMORY_MB}'
    GALAXY_SLOTS: '{PARALLELISATION}'
    LC_ALL: C
  params:
    nativeSpecification: '-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    require_container: true
    singularity_enabled: true
    singularity_cmd: /local/singularity/3.8.0/bin/singularity
    singularity_volumes: '$defaults,/groups/:ro,/db/:ro,/scratch/:ro'
    container:
      - type: singularity
        shell: '/bin/sh'
        resolve_dependencies: true
        identifier: '/cvmfs/singularity.galaxyproject.org/all/centos:8.3.2011'

## Run a tool with Docker within a Slurm job
slurm_docker:
  info:
    remote: 'False'
    scheduler: 'slurm'
  limits:
    cores: 16
    mem: 31
  env: { }
  params:
    nativeSpecification: '-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}'
    docker_enabled: true
    require_container: true
    docker_volumes: '$defaults'
    docker_memory: '{MEMORY}'
    docker_auto_rm: true
    docker_default_container_id: busybox:ubuntu-14.04
    docker_sudo: false
    docker_set_user: ''
    tmp_dir: 'True'
